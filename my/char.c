/*
** char.c for minitalk in /home/blackbird/work/minitalk
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Tue Mar 18 18:30:32 2014 romaric
** Last update Tue Mar 18 18:30:35 2014 romaric
*/

#include "minitalk.h"

int     my_strlen(char *str)
{
  int   x;

  x = 0;
  while (str[x] != '\0')
    x++;
  return (x);
}

int	my_putstr(char *str, int op)
{
  return (write(op, str, my_strlen(str)));
}
