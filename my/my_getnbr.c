/*
** my_getnbr.c for minitalk in /home/blackbird/work/minitalk
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Tue Mar 18 12:15:44 2014 romaric
** Last update Tue Mar 18 18:21:28 2014 romaric
*/

#include "minitalk.h"

int     my_getnbr(char *str)
{
  int   cpt;
  int   i;
  int   result;

  if (str == NULL)
    return (0);
  result = 0;
  i = 0;
  cpt = 1;
  while (str[i] >= 48 && str[i] <= 57)
    {
      cpt = cpt * 10;
      i++;
    }
  cpt = cpt / 10;
  i = 0;
  while (str[i] >= 48 && str[i] <= 57)
    {
      result = result + ((str[i] - 48) * cpt);
      cpt = cpt / 10;
      i++;
    }
  return (result);
}
