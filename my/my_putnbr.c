/*
** my_putnbr.c for minitalk in /home/blackbird/work/minitalk
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Tue Mar 18 18:20:19 2014 romaric
** Last update Tue Mar 18 20:47:25 2014 romaric
*/

#include "minitalk.h"

void    my_putnbr(int n)
{
  if (n < 0)
    {
      write(1, "-", 1);
      n = -n;
    }
  if (n >= 10)
    {
      my_putnbr(n / 10);
      my_putnbr(n % 10);
    }
  else
    {
      n = n + 48;
      write(1, &n, 1);
    }
}
