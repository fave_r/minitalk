/*
** serveur.c for minitalk in /home/blackbird/work/minitalk
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Mon Mar 17 16:26:39 2014 romaric
** Last update Sun Mar 23 15:25:34 2014 romaric
*/

#include "minitalk.h"

void	message(int pid, char a)
{
  char	b;

  b = 0;
  while (b < 8)
    {
      if (((a >> b) & 1) == 1)
	{
	  if (kill(pid, SIGUSR1) == -1)
	    {
	      my_putstr("\033[31m\nKILL FAIL !\n\033[0;m", 2);
	      exit(EXIT_FAILURE);
	    }
	}
      else
	{
	  if (kill(pid, SIGUSR2) == -1)
	    {
	      my_putstr("\033[31m\nKILL FAIL !\n\033[0;m", 2);
	      exit(EXIT_FAILURE);
	    }
	}
      usleep(500);
      b++;
    }
}

int	main(int ac, char **av)
{
  int	i;

  i = 0;
  if (ac < 3)
    {
      my_putstr("\033[31musage: ./client PID STR\n\033[0;m", 2);
      exit(EXIT_FAILURE);
    }
  if (ac == 3)
    {
      while (av[2][i])
	{
	  message(my_getnbr(av[1]), av[2][i]);
	  i++;
	}
      message(my_getnbr(av[1]), av[2][i]);
    }
  return (0);
}
