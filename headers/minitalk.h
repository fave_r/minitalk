/*
** minitalk.h for minitalk in /home/blackbird/rendu/PSU_2013_minitalk/headers
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Mar 23 15:27:06 2014 romaric
** Last update Sun Mar 23 15:27:11 2014 romaric
*/

#ifndef __MINITALK__
#define __MINITALK__

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

void    my_putnbr(int n);
int     my_getnbr(char *str);
int     my_strlen(char *str);
int     my_putstr(char *str, int op);

#endif
