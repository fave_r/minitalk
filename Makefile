##
## Makefile for minitalk in /home/blackbird/work/minitalk
##
## Made by romaric
## Login   <fave_r@epitech.net>
##
## Started on  Mon Mar 17 16:22:26 2014 romaric
## Last update Sun Mar 23 14:05:07 2014 romaric
##

CC=		gcc

RM=		rm -f

CFLAGS=		-Wextra -Wall -Werror -I./headers

NAME_CLIENT=	client/client

NAME_SERVER=	server/server

SRCS_CLIENT=	./client/client.c \
		./my/char.c \
		./my/my_getnbr.c

SRCS_SERVER=	./server/serveur.c \
		./my/my_putnbr.c

OBJS_CLIENT=	$(SRCS_CLIENT:.c=.o)

OBJS_SERVER=	$(SRCS_SERVER:.c=.o)

all: $(NAME_CLIENT) $(NAME_SERVER)

$(NAME_CLIENT): $(OBJS_CLIENT)
		$(CC) $(OBJS_CLIENT) -o $(NAME_CLIENT) $(LDFLAGS)

$(NAME_SERVER): $(OBJS_SERVER)
		$(CC) $(OBJS_SERVER) -o $(NAME_SERVER) $(LDFLAGS)

clean:
	$(RM) $(OBJS_CLIENT)
	$(RM) $(OBJS_SERVER)

fclean:	clean
	$(RM) $(NAME_CLIENT)
	$(RM) $(NAME_SERVER)

re: fclean all

.PHONY:	all fclean re