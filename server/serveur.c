/*
** my_signal.c for minitalk in /home/blackbird/work/minitalk
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Mon Mar 17 17:20:21 2014 romaric
** Last update Thu Mar 20 11:45:56 2014 romaric
*/

#include "minitalk.h"

void	writealet(int bool)
{
  static char	a = 0;
  static int	i = 0;

  a += (bool << i++);
  if (i == 8)
    {
      write(1, &a, 1);
      a = 0;
      i = 0;
    }
}

void	handle_signal(int signo)
{
  if (signo == SIGUSR1)
    writealet(1);
  if (signo == SIGUSR2)
    writealet(0);
}

int	main(void)
{
  my_putnbr(getpid());
  write(1, "\n", 1);
  signal(SIGUSR1, handle_signal);
  signal(SIGUSR2, handle_signal);
  while (42)
    pause();
  return (0);
}
